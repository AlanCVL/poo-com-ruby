require 'date'
class Usuarios
    attr_accessor :email, :senha, :nome, :nascimento, :logado

    #Ja inicializo as variaveis com determinado valores já pre-fixados
    def initialize(email="",  senha = "",  nome="",  nascimento="",  logado="false")
        @email=email
        @senha=senha
        @nome=nome
        @nascimento = nascimento
        @logado=false
    end

    #Calcula a idade do usuario baseando-se na data de nascimento
    def idade()
        
        #separa os valores digitados na data do nascimento em dia, mes e ano
        dia_nascimento =  @nascimento[0,2].to_i
        mes_nascimento =  @nascimento[3,2].to_i
        ano_nascimento =  @nascimento[6,4].to_i

        #Aloca os valores da data atual em variaveis em dia, mes, ano
        dia_atual= Time.now.strftime("%d").to_i
        mes_atual = Time.now.strftime("%m").to_i
        ano_atual = Time.now.strftime("%Y").to_i

        #subtrai o ano atual - ano de nascimento
        idade =  ano_atual - ano_nascimento

        #verifica se temos que subtrair um (1)
        #se o mes atual é menor que o seu mes de aniversario, seu mes de aniversario, ainda nao chegou
        if mes_atual < mes_nascimento
            idade-=1
        
        else
            #se o mes atual de aniversario é igual ao mes atual, mas a data atual é menor que a data do seu aniversario, falta poucos dias para seu aniversario, mas mesmo assim você ainda não fez
            if mes_atual == mes_nascimento && dia_atual < dia_nascimento
                idade-=1
            end
        end


        puts "A idade do usuario é #{idade}"
    end

    #Realiza o login do usuario
    def logar(senha)
        #Se ele ja esta logado nao necessita logar novamente
        if @logado == true
            puts 'Você já esta logado'
            return
        end

        #se a senha for igual senha já pre-definida o Usuario loga
        if senha==@senha
            @logado=true
            puts 'Senha correta'
            puts 'Logado'
            puts "Bem vindo ao Sistema de Alunos"
            return

        else #Caso contrarrio, se as senhas forem diferentes ele nao loga
            puts 'Senha incorreta'
            @logado = false
        end
    end

    #desloga
    def deslogar
        puts 'Você foi desconectado ao Sistema de Alunos'
        @logado = false
    end
end



class Aluno < Usuarios
    attr_accessor :matricula, :periodo, :curso, :turmas

    #Ja inicializo as variaveis com determinado valores já pre-fixados
    def initialize(matricula="", periodo='', curso='', turmas=[])
        #Chama os atributos e metodos da classe Usuarios
        super 

        @matricula = matricula
        @periodo = periodo
        @curso = curso
        @turmas = turmas
    end

    #Aluno se inscreve na turma
    def inscrever(nome_turma)
        #Se o nome_turma já estiver no array turmas, da um return
        if turmas.include?(nome_turma)
            puts "A turma #{nome_turma} já foi cadastrado no nosso Banco de Dados"
            return
        end

        #Se tentar se inscrever na turma mesmo sem ter digitado nada
        if nome_turma ==''
            puts 'Nenhum valor foi inserido, por favor digite algo'
            return
        
        #Se a nova_turma nao estiver no array turmas
        else
            turmas.append(nome_turma)
            puts "Foi adcionado a grade a materia #{nome_turma}"
            puts "As turmas ja inscritas são:"
            #Mostra as turmas já cadastradas pelo aluno
            for i in  turmas
                puts '* '+ i
            end
        end
    end

    #Aluno se desinscreve na turma
    def desinscrever(nome_turma)
        #Se nome_turma estiver no array turmas, será removido
        if turmas.include?(nome_turma)
            turmas.delete(nome_turma)
            puts "A turma #{nome_turma} foi removido com sucesso"
        #Caso contrario, avisará que não está 
        else
            puts "A turma #{nome_turma} não esta cadastrada no nosso Banco de Dados"
        end

    end 
end

class Professor < Usuarios
    attr_accessor :matricula, :salario, :materias
    def initialize(matricula="", salario= 0.00, materias=[])
        super
        @matricula = matricula
        @salario = salario
        @materias = materias
    end

    #Professor se inscreve na materia
    def adicionar_materia(nome_materia)
        #Se o nome_materia já estiver no array materias, da um return
        if materias.include?(nome_materia)
            puts "A materia #{nome_materia} já foi cadastrado no nosso Banco de Dados"
            return
        end

        #Se tentar se inscrever materia mesmo sem ter digitado nada
        if nome_materia ==''
            puts 'Nenhum valor foi inserido, por favor digite algo'
            return

        #Se a nova_materia nao estiver no array materias
        else
            materias.append(nome_materia)
            puts "Foi adcionado a grade a materia #{nome_materia}"
            puts "As materias ja inscritas são:"
            #Mostra as materias já cadastradas pelo professor
            for i in  materias
                puts '* '+ i
            end
        end
    end

    #Professor se desinscreve na materia
    def remove_materia(nome_materia)
        #Se nome_materia estiver no array materias, será removido
        if materias.include?(nome_materia)
            materias.delete(nome_materia)
            puts "A materia #{nome_materia} foi removido com sucesso"
        #Caso contrario, avisará que não está 
        else
            puts "A materia #{nome_materia} não esta cadastrada no nosso Banco de Dados"
        end

    end 
end

