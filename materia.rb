class Materia
    attr_accessor :ementa, :nome, :professores
    def initialize(ementa='', nome='', professores=[])
        @ementa = ementa
        @nome = nome
        @professores = professores
    end

    #Professor se inscreve na materia
    def adicionar_professor(nome_professor)
        #Se o nome_professor já estiver no array professores, da um return
        if professores.include?(nome_professor)
            puts "O professor #{nome_professor} já foi cadastrado no nosso Banco de Dados"
            return
        end
        #Se tentar adicionar professor mesmo sem ter digitado nada
        if nome_professor ==''
            puts 'Nenhum valor foi inserido, por favor digite algo'
            return
        #Se o nova_professor nao estiver no array professores
        else
            professores.append(nome_professor)
            puts "Foi adcionado ao time de professores, o professor #{nome_professor}"
            puts "Os professores ja inscritos são:"
            #Mostra os professores já cadastradas no array professores
            for i in  professores
                puts '* '+ i
            end
        end
    end

    #Materia desinscreve professor
    def remove_professor(nome_professor)
        #Se nome_professor estiver no array professores, será removido
        if professores.include?(nome_professor)
            professores.delete(nome_professor)
            puts "O professor #{nome_professor} foi removido com sucesso"
        #Caso contrario, avisará que não está 
        else
            puts "O professor #{nome_professor} não esta cadastrada no nosso Banco de Dados"
        end

    end 
    
end
