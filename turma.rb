class Turma
    attr_accessor :nome, :horario, :dias_da_semana, :inscritos, :inscricao_aberta
    def initialize(nome = '', horario='', dias_da_semana = [], inscritos=[], inscricao_aberta=false)
        @nome = nome
        @horario = horario
        @dias_da_semana = dias_da_semana
        @inscritos = inscritos
        @inscricao_aberta = inscricao_aberta
    end

    def abrir_inscricao
        @inscricao_aberta = true
    end

    def fechar_inscricao
        @inscricao_aberta = false
    end
    
    #Aluno se inscreve na turma
    def adicionar_aluno(nome_aluno)
        #Se o nome_aluno já estiver no array inscritos, da um return
        if inscritos.include?(nome_aluno)
            puts "O aluno #{nome_aluno} já foi cadastrado no nosso Banco de Dados"
            return
        end
        #Se tentar inscrever aluno mesmo sem ter digitado nada
        if nome_aluno ==''
            puts 'Nenhum valor foi inserido, por favor digite algo'
            return
        #Se o nova_aluno nao estiver no array inscritos
        else
            inscritos.append(nome_aluno)
            puts "Foi adcionado a turma o aluno #{nome_aluno}"
            puts "Os alunos ja inscritos são:"
            #Mostra os alunos  já cadastradas no array inscritos
            for i in  inscritos
                puts '* '+ i
            end
        end
    end

    #Turma desinscreve alunos cadastrados
    def remove_aluno(nome_aluno)
        #Se nome_aluno estiver no array inscritos, será removido
        if inscritos.include?(nome_aluno)
            inscritos.delete(nome_aluno)
            puts "O aluno #{nome_aluno} foi removido com sucesso"
        #Caso contrario, avisará que não está 
        else
            puts "O aluno #{nome_aluno} não esta cadastrada no nosso Banco de Dados"
        end

    end 
    
end
